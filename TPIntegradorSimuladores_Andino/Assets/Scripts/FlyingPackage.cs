using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingPackage : MonoBehaviour
{
    public bool fly = false;
    public Transform forcePos;
    Vector3 force = new Vector3(0, 10, 0);

    private void Update()
    {
        if(Input.GetKey(KeyCode.K))
        {
            StartCoroutine("Fly");            
        }
        if(fly)
        {
            this.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(force, forcePos.position, ForceMode.Acceleration);
        }
    }
    IEnumerator Fly()
    {
        fly = true;
        yield return new WaitForSeconds(4);
        fly = false;
        yield break;
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.impulse.y > 5 || collision.impulse.x > 5 || collision.impulse.z > 5)
    //    {
    //        StartCoroutine("Fly");
    //    }
    //}
}
