using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel_Steering : MonoBehaviour
{
    RaycastHit hit;
    bool RayCastDidHit = false;

    Rigidbody rbCar;

    float wheelMass = 0.8f;
    float wheelGripFactor = 0.7f;


    private void Start()
    {
        rbCar = gameObject.GetComponentInParent<Rigidbody>();
    }

    void FixedUpdate()
    {
        RayCastDidHit = Physics.Raycast(transform.position, -transform.up, out hit, 1.5f);
        float inputHor = Input.GetAxis("Horizontal");
        if (RayCastDidHit)
        {
            Vector3 steeringDir = transform.right;

            Vector3 wheelWorldVel = rbCar.GetPointVelocity(transform.position);

            float steeringVel = Vector3.Dot(steeringDir, wheelWorldVel);

            float desiredVelChange = -steeringVel * wheelGripFactor;

            float desiredAccel = desiredVelChange / Time.fixedDeltaTime;

            rbCar.AddForceAtPosition(steeringDir * wheelMass * desiredAccel, transform.position);
         
        }
        
    }
}
