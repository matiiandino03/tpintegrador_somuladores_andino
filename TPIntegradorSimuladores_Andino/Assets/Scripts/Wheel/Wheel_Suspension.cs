using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel_Suspension : MonoBehaviour
{
    RaycastHit hit;
    bool RayCastDidHit = false;

    Rigidbody rbCar;

    public float suspensionRestDist = 1.5f;
    float springDamper = 100;
    float springStrength = 600;

    private void Start()
    {
        rbCar = gameObject.GetComponentInParent<Rigidbody>();
    }

    void FixedUpdate()
    {
        RayCastDidHit = Physics.Raycast(transform.position, -transform.up, out hit, 1.5f);
        if (RayCastDidHit)
        {
            Vector3 springDir = transform.up;

            Vector3 wheelWorldVel = rbCar.GetPointVelocity(transform.position);

            float offset = suspensionRestDist - hit.distance;

            float vel = Vector3.Dot(springDir, wheelWorldVel);

            float force = (offset * springStrength) - (vel * springDamper);

            rbCar.AddForceAtPosition(springDir * force, transform.position);

        }        
    }

}
