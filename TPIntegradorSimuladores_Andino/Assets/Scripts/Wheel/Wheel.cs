using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour
{
    RaycastHit hit;
    bool RayCastDidHit = false;
    Rigidbody rbCar;
    #region Suspension Variables
    float suspensionRestDist = 1.5f;
    float springDamper = 100;
    float springStrength = 600;
    #endregion
    #region Doblar Variables
    float wheelMass = 2;
    float wheelGripFactor;
    #endregion
    private void Start()
    {
        rbCar = gameObject.GetComponentInParent<Rigidbody>();
    }

    void Update()
    {
        RayCastDidHit = Physics.Raycast(transform.position, -transform.up, out hit, 1.5f);
        //Suspension de cada rueda
        if(RayCastDidHit)
        {
            Vector3 springDir = transform.up;

            Vector3 wheelWorldVel = rbCar.GetPointVelocity(transform.position);

            float offset = suspensionRestDist - hit.distance;

            float vel = Vector3.Dot(springDir,wheelWorldVel);

            float force = (offset * springStrength) - (vel * springDamper);

            rbCar.AddForceAtPosition(springDir * force, transform.position);
        }
        //Como se comporta el auto cuando dobla
        if(RayCastDidHit)
        {
            Vector3 steeringDir = transform.right;

            Vector3 wheelWorldVel = rbCar.GetPointVelocity(transform.position);

            float steeringVel = Vector3.Dot(steeringDir,wheelWorldVel);

            float desiredVelChange = -steeringVel * wheelGripFactor;

            float desiredAccel = desiredVelChange / Time.fixedDeltaTime;

            rbCar.AddForceAtPosition(steeringDir * wheelMass * desiredAccel, transform.position);

        }
    }
}
