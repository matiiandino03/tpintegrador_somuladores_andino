using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    public GameObject pauseMenu;
    public Text txtMissionsCompleted;
    public static int missionsCompleted = 0;
    public GameObject Helicopter;
    public Transform HeliSpawn;
    
    private void Awake()
    {
        missionsCompleted = 0;
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
        }

        if(Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(1);
        }

        txtMissionsCompleted.text = missionsCompleted.ToString();
    }

    public void Resume()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void Exit()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(0);
        pauseMenu.SetActive(false);
    }

    public void NewHeli()
    {
        Helicopter.transform.position = HeliSpawn.transform.position;
        Helicopter.transform.rotation = Quaternion.identity;
    }
}
