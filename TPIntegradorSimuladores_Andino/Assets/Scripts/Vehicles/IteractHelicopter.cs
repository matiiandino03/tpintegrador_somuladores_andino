using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IteractHelicopter : MonoBehaviour
{
    public GameObject Ragdoll;

    public bool canDrive = false;
    public bool driving = false;


    public HelicopterController wheelControl1;
    public HelicopterController wheelControl2;
    public HelicopterController wheelControl3;
    public HelicopterController wheelControl4;
    public HelicopterMouseControl mouseControl;


    public RagdollPlayer_Controller rpc;
    public Grab[] grab;
    public Copy_Motion[] cm;

    public GameObject handle;

    public CameraController cc;

    private void Start()
    {
        grab = Ragdoll.GetComponentsInChildren<Grab>();
        cm = Ragdoll.GetComponentsInChildren<Copy_Motion>();
        rpc = Ragdoll.GetComponentInChildren<RagdollPlayer_Controller>();
    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) && driving == true)
        {
            wheelControl1.isDriving = false;
            wheelControl2.isDriving = false;
            wheelControl3.isDriving = false;
            wheelControl4.isDriving = false;
            mouseControl.isDriving = false;

            foreach (Copy_Motion copy in cm)
            {
                copy.enabled = true;
            }
            foreach (Grab gb in grab)
            {
                gb.enabled = true;
                Destroy(gb.fj);
            }
            cc.isDriving = false;
            rpc.enabled = true;
            driving = false;
        }
        else if (canDrive && Input.GetKeyDown(KeyCode.E) && driving == false)
        {
            FixedJoint[] fjs = handle.GetComponents<FixedJoint>();
            if (fjs.Length == 2)
            {
                wheelControl1.isDriving = true;
                wheelControl2.isDriving = true;
                wheelControl3.isDriving = true;
                wheelControl4.isDriving = true;
                mouseControl.isDriving = true;

                foreach (Copy_Motion copy in cm)
                {
                    copy.enabled = false;
                }
                foreach (Grab gb in grab)
                {
                    gb.enabled = false;
                }
                cc.isDriving = true;
                rpc.enabled = false;
                driving = true;
                DrivingVehicle();
            }
        }
    }

    public void DrivingVehicle()
    {
        FindObjectOfType<MisionsManager>().thirdClue.SetActive(false);
    }
}
