using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanDrive : MonoBehaviour
{
    public bool Car;
    public bool forklift;
    public bool isFirst = true;
    public void OnVehicle()
    {
        if(isFirst)
        {
            FindObjectOfType<MisionsManager>().thirdClue.SetActive(true);
            FindObjectOfType<MisionsManager>().firstClue.SetActive(false);
            FindObjectOfType<MisionsManager>().secondClue.SetActive(false);
            isFirst = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ragdoll") && Car)
        {
            OnVehicle();
            other.GetComponentInParent<Interact>().canDrive = true;
        }
        if (other.CompareTag("Ragdoll") && !Car && !forklift)
        {
            OnVehicle();
            other.GetComponentInParent<IteractHelicopter>().canDrive = true;
        }
        if (other.CompareTag("Ragdoll") && forklift)
        {
            OnVehicle();
            other.GetComponentInParent<InteractForklift>().canDrive = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ragdoll") && Car)
        {
            other.GetComponentInParent<Interact>().canDrive = false;
        }
        if (other.CompareTag("Ragdoll") && !Car && !forklift)
        {
            other.GetComponentInParent<IteractHelicopter>().canDrive = false;
        }
        if (other.CompareTag("Ragdoll") && forklift)
        {
            other.GetComponentInParent<InteractForklift>().canDrive = false;
        }
    }
}
