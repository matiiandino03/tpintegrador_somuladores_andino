using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractForklift : MonoBehaviour
{
    public GameObject Ragdoll;

    public bool canDrive = false;
    public bool driving = false;


    public Wheel_Acceleration wheelControl1;
    public Wheel_Acceleration wheelControl2;


    public RagdollPlayer_Controller rpc;
    public Grab[] grab;
    public Copy_Motion[] cm;

    public GameObject handle;

    public GameObject Horizontal_R;
    public GameObject Horizontal_L;

    private void Start()
    {
        grab = Ragdoll.GetComponentsInChildren<Grab>();
        cm = Ragdoll.GetComponentsInChildren<Copy_Motion>();
        rpc = Ragdoll.GetComponentInChildren<RagdollPlayer_Controller>();
    }
    void Update()
    {
        if(driving == true && Input.GetMouseButton(0) && Horizontal_R.transform.position.y < 2.5f && Horizontal_L.transform.position.y < 2.5f)
        {
            Horizontal_R.transform.position += -Horizontal_R.transform.forward * Time.fixedDeltaTime;
            Horizontal_L.transform.position += -Horizontal_L.transform.forward * Time.fixedDeltaTime;
        }
        if (driving == true && Input.GetMouseButton(1) && Horizontal_R.transform.localPosition.y > -1.73f && Horizontal_L.transform.localPosition.y > -1.73f)
        {
            Horizontal_R.transform.position += Horizontal_R.transform.forward * Time.fixedDeltaTime;
            Horizontal_L.transform.position += Horizontal_L.transform.forward * Time.fixedDeltaTime;
        }
        if (Input.GetKeyDown(KeyCode.E) && driving == true)
        {
            wheelControl1.isDriving = false;
            wheelControl2.isDriving = false;
            foreach (Copy_Motion copy in cm)
            {
                copy.enabled = true;
            }
            foreach (Grab gb in grab)
            {
                gb.enabled = true;
                Destroy(gb.fj);
            }
            rpc.enabled = true;
            driving = false;
        }
        else if (canDrive && Input.GetKeyDown(KeyCode.E) && driving == false)
        {
            FixedJoint[] fjs = handle.GetComponents<FixedJoint>();
            if (fjs.Length == 2)
            {
                wheelControl1.isDriving = true;
                wheelControl2.isDriving = true;
                foreach (Copy_Motion copy in cm)
                {
                    copy.enabled = false;
                }
                foreach (Grab gb in grab)
                {
                    gb.enabled = false;
                }
                rpc.enabled = false;
                driving = true;
                DrivingVehicle();
            }
        }

    }

    public void DrivingVehicle()
    {
        FindObjectOfType<MisionsManager>().thirdClue.SetActive(false);
    }
}
