using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterController : MonoBehaviour
{
    Rigidbody rbHelicopter;

    float upForce = 300;
    float downForce = 80;
    public bool isLeft;

    public bool isFront;

    public bool isDriving;
    public bool RayCastDidHit = false;

    void Start()
    {
        rbHelicopter = gameObject.GetComponentInParent<Rigidbody>();
    }

    
    void FixedUpdate()
    {
        RayCastDidHit = Physics.Raycast(transform.position, -transform.up, out RaycastHit hit, 4.2f);
        float inputVertical = Input.GetAxis("Vertical");
        float inputHorizontal = Input.GetAxis("Horizontal");
        if (inputVertical > 0 && isDriving)
        {            
            Vector3 forceDir = transform.up;

            Vector3 wheelWorldVel = rbHelicopter.GetPointVelocity(transform.position);

            float vel = Vector3.Dot(forceDir, wheelWorldVel);

            float force;
            if(isFront)
            {
                if (isLeft)
                {
                    force = ((inputVertical * upForce) - (vel * 50)) - (inputHorizontal * -100);
                }
                else
                {
                    force = ((inputVertical * upForce) - (vel * 50)) - (inputHorizontal * 100);
                }
            }
            else
            {
                force = ((inputVertical * upForce) - (vel * 50));

            }

            rbHelicopter.AddForceAtPosition(forceDir * force,transform.position);
        }
        if (inputVertical < 0 && isDriving)
        {
            Vector3 forceDir = transform.up;

            Vector3 wheelWorldVel = rbHelicopter.GetPointVelocity(transform.position);

            float vel = Vector3.Dot(forceDir, wheelWorldVel);

            float force;
            if (isFront)
            {
                if (isLeft)
                {
                    force = ((inputVertical * downForce) - (vel * 50)) - (inputHorizontal * -100);
                }
                else
                {
                    force = ((inputVertical * downForce) - (vel * 50)) - (inputHorizontal * 100);
                }
            }
            else
            {
                force = ((inputVertical * downForce) - (vel * 50));

            }

            rbHelicopter.AddForceAtPosition(forceDir * force, transform.position);
        }

        if(!isDriving && RayCastDidHit)
        {
            
            rbHelicopter.velocity = Vector3.zero;
        }
    }
}
