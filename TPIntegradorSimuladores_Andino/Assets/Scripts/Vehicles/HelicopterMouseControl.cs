using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterMouseControl : MonoBehaviour
{
    float inputMouseY;
    float inputMouseX;
    float inputHor;
    public bool isDriving;
    public GameObject Rotor;

    void Update()
    {
        if (isDriving)
        {
            Rotor.transform.Rotate(new Vector3(0, 10, 0));

            inputMouseY += Input.GetAxis("Mouse Y") * 90 * Time.deltaTime;
            inputMouseX += Input.GetAxis("Mouse X") * 90 * Time.deltaTime;
            inputHor += Input.GetAxis("Horizontal") * 50 * Time.deltaTime;
            inputMouseY = Mathf.Clamp(inputMouseY, -20, 25);
            inputMouseX = Mathf.Clamp(inputMouseX, -30, 30);
            transform.rotation = Quaternion.Euler(inputMouseY, transform.rotation.y + inputHor, -inputMouseX);
        }

    }
}
