using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab : MonoBehaviour
{
    public Animator animator;
    public GameObject grabedObject;

    public Rigidbody rb;
    public int isLeftorRight;
    public bool alreadyGrabbing = false;
    public bool canGrab = false;
    public SphereCollider sc;
    public FixedJoint fj;
    public FixedJoint[] fjs;

    public GameObject secondClue;
    public GameObject firstClue;

    public bool isFirst = true;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        sc = GetComponent<SphereCollider>();
    }

    private void Update()
    {
        //fjs = FindObjectsOfType<FixedJoint>();
        if (Input.GetMouseButtonDown(isLeftorRight))
        {
            if(isLeftorRight == 0)
            {
                animator.SetBool("isLeftHandUp",true);
            }
            if (isLeftorRight == 1)
            {
                animator.SetBool("isRightHandUp", true);
            }
        }

        if(canGrab && Input.GetMouseButton(isLeftorRight))
        {
            if (grabedObject != null && fj == null)
            {
                canGrab = false;
                fj = grabedObject.AddComponent<FixedJoint>();
                fj.connectedBody = rb;
                fj.breakForce = 9000 + isLeftorRight;
                if(isFirst)
                {
                    secondClue.SetActive(true);
                    firstClue.SetActive(false);
                    FindObjectOfType<MisionsManager>().thirdClue.SetActive(false);
                    isFirst = false;
                }

            }  
        }

        if(Input.GetMouseButtonUp(isLeftorRight))
        {
               Destroy(fj);
               grabedObject = null;
               sc.enabled = false;
               sc.enabled = true;
            fjs = FindObjectsOfType<FixedJoint>();
            foreach (FixedJoint fixjoint in fjs)
            {
                if(fixjoint.breakForce == 9000+isLeftorRight)
                {
                    Destroy(fixjoint);
                }              
            }
            fjs = null;
        }

        if(Input.GetMouseButtonUp(isLeftorRight))
        {
            if (isLeftorRight == 0)
            {
                animator.SetBool("isLeftHandUp", false);
            }
            else if (isLeftorRight == 1)
            {
                animator.SetBool("isRightHandUp", false);
            }          
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("item"))
        {
            grabedObject = other.gameObject;
            canGrab = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        grabedObject = null;
        canGrab = false;
    }
}
