using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Copy_Motion : MonoBehaviour
{
    public Transform targetLimb;
    ConfigurableJoint cj;
    private void Start()
    {
        cj = GetComponent<ConfigurableJoint>();
    }

    private void Update()
    {
        cj.targetRotation = targetLimb.rotation;
    }
}
