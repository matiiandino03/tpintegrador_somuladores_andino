using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    public float rotationSpeed = 1;
    public Transform cameraCenter;

    float mouseX, mouseY;

    public float stomachOffset;

    public ConfigurableJoint hipJoint, stomachJoint;

    public bool isPlayer;

    public bool isDriving;


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(1);
        }
    }

    private void FixedUpdate()
    {
        CameraControl();
    }   
    void CameraControl()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY += Input.GetAxis("Mouse Y") * rotationSpeed;
        mouseY = Mathf.Clamp(mouseY,-35,60);

        if(isDriving)
        {
            Quaternion cameraCenterRotation = Quaternion.Euler(0, mouseX, 0);

            cameraCenter.rotation = cameraCenterRotation;
        }
        else
        {
            Quaternion cameraCenterRotation2 = Quaternion.Euler(0, mouseX, -mouseY);

            cameraCenter.rotation = cameraCenterRotation2;
        }       

        if(isPlayer)
        {
            hipJoint.targetRotation = Quaternion.Euler(0, -mouseX, 0);
            stomachJoint.targetRotation = Quaternion.Euler(0, 0, mouseY + stomachOffset);
        }

    }
}
