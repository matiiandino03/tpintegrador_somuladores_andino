using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollPlayer_Controller : MonoBehaviour
{
    public float speed;
    public float strafeSpeed;
    public float jumpForce;

    public Rigidbody rbHips;

    public Animator animator;

    public GameObject feet_L, feet_R;

    RaycastHit hit;

    bool RayCastDidHit_L;
    bool RayCastDidHit_R;
    void Start()
    {
        rbHips = GetComponent<Rigidbody>();
        animator = FindObjectOfType<Animator>();
    }

    private void Update()
    {
        RayCastDidHit_L = Physics.Raycast(feet_L.transform.position, -feet_L.transform.up, out hit, 0.2f);
        RayCastDidHit_R = Physics.Raycast(feet_R.transform.position, -feet_R.transform.up, out hit, 0.2f);
    }
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.W))
        {
            animator.SetBool("IsWalk", true);
            if (Input.GetKey(KeyCode.LeftShift))
            {
                rbHips.AddForce(transform.right * speed * 1.5f);
                
            }
            else 
            {
                rbHips.AddForce(transform.right * speed );
            }

        }

        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("IsWalk", true);
            rbHips.AddForce(-transform.right * speed);
        }
        if(!Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.W)) 
        { animator.SetBool("IsWalk", false); }

        if (Input.GetKey(KeyCode.A))
        {
            rbHips.AddForce(transform.forward * strafeSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rbHips.AddForce(-transform.forward * strafeSpeed);
        }

        if(Input.GetAxis("Jump") > 0 && RayCastDidHit_L && RayCastDidHit_R)
        {

            rbHips.AddForce(new Vector3(0, jumpForce, 0));
            
        }
    }
}
