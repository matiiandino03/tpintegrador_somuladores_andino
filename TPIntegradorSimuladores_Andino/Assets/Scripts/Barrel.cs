using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    public ParticleSystem ps1;
    public ParticleSystem ps2;
    public ParticleSystem ps3;

    List<Rigidbody> rbs = new List<Rigidbody>();

    bool exploted = false;

    void Start()
    {

    }       

    void Update()
    {
        if(exploted && ps1.isStopped && ps2.isStopped)
        {
            FindObjectOfType<MisionsManager>().GenerateNewMision();
            Destroy(this.gameObject);
        }
    }
    public void Explosion()
    {
        ps1.Play();
        ps2.Play();
        ps3.Play();

        foreach(Rigidbody rb in rbs)
        {
            rb.AddExplosionForce(400, transform.position, 6.59f);
        }
        exploted = true;
        this.gameObject.GetComponent<MeshRenderer>().enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Rigidbody>() != null)
        {
            rbs.Add(other.GetComponent<Rigidbody>());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Rigidbody>() != null)
        {
            rbs.Remove(other.GetComponent<Rigidbody>());
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.impulse.y > 5 || collision.impulse.x > 5 || collision.impulse.z > 5)
        {
            Explosion();
        }
    }
}
