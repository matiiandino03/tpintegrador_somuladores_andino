using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MisionWaypoint : MonoBehaviour
{
    public Image img;
    public Image imgPackage;
    public Transform target;
    public Transform targetPackage;
    public Text distance;
    public Transform player;

    private void Update()
    {
        float minX = img.GetPixelAdjustedRect().width / 2;
        float maxX = Screen.width - minX;

        float minY = img.GetPixelAdjustedRect().height / 2;
        float maxY = Screen.height - minY;

        Vector3 pos = Camera.main.WorldToScreenPoint(target.position);
        Vector3 pos2 = Camera.main.WorldToScreenPoint(targetPackage.position);

        //if (Vector3.Dot((target.position - player.transform.position), player.transform.forward) < 0 )
        if (pos.z < 0)
        {
            if (pos.x < Screen.width / 2)
            {
                pos.x = maxX;
            }
            else
            {
                pos.x = minX;
            }
        }
        if (pos2.z < 0)
        {
            if (pos2.x < Screen.width / 2)
            {
                pos2.x = maxX;
            }
            else
            {
                pos2.x = minX;
            }
        }

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);

        pos2.x = Mathf.Clamp(pos2.x, minX, maxX);
        pos2.y = Mathf.Clamp(pos2.y, minY, maxY);

        img.transform.position = pos;
        imgPackage.transform.position = pos2;
        distance.text = ((int)(Vector3.Distance(target.position, player.transform.position))).ToString() + "m";

    }

    public void NotInMission()
    {
        img.enabled = false;
        imgPackage.enabled = false;
        distance.enabled = false;
    }
    public void InMission()
    {
        img.enabled = true;
        imgPackage.enabled = true;
        distance.enabled = true;
    }
}
