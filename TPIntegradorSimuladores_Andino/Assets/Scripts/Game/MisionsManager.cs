using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisionsManager : MonoBehaviour
{
    public GameObject[] Packages;
    public GameObject[] Targets;

    public Transform spawnPackages;

    public MisionWaypoint mw;

    GameObject package;
    GameObject target;

    public GameObject firstClue;
    public GameObject secondClue;
    public GameObject thirdClue;


    private void Start()
    {
        GenerateFirstMission();
    }


    public void NeedHelp()
    {
        firstClue.SetActive(true);
        secondClue.SetActive(false);
        thirdClue.SetActive(false);
        Grab[] grabs = FindObjectsOfType<Grab>();
        foreach(CanDrive c in FindObjectsOfType<CanDrive>())
        {
            c.isFirst = true;
        }
        foreach (Grab g in grabs)
        {
            g.isFirst = true;
        }
    }

    public void GenerateNewMision()
    {
        int i = Random.Range(0, Packages.Length);
        int h = Random.Range(0, Targets.Length);

        target = Targets[h];

        package = Instantiate(Packages[i], spawnPackages.transform.position, Quaternion.identity);

        Targets[h].SetActive(true);
        mw.target = Targets[h].transform;
        mw.targetPackage = package.transform;
        mw.InMission();
    }

    public void GenerateFirstMission()
    {
        firstClue.SetActive(true);
        target = Targets[0];
        Targets[0].SetActive(true);
        mw.target = Targets[0].transform;        
        package = Instantiate(Packages[0], spawnPackages.transform.position, Quaternion.identity);
        mw.targetPackage = package.transform;
        mw.InMission();
    }

    public void EndMision()
    {
        firstClue.SetActive(false);
        secondClue.SetActive(false);
        thirdClue.SetActive(false);
        Destroy(package);
        target.SetActive(false);
        target = null;
        mw.NotInMission();
        GenerateNewMision();
    }
    
}
