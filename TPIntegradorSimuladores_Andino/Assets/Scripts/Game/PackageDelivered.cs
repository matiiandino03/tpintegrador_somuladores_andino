using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackageDelivered : MonoBehaviour
{
    bool delete;
    private void Update()
    {
        if(delete && this.gameObject.GetComponent<ParticleSystem>().isStopped)
        {
            FindObjectOfType<MisionsManager>().EndMision();
            CanvasManager.missionsCompleted++;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("item"))
        {
            this.gameObject.GetComponent<ParticleSystem>().Play();
            delete = true;
            
        }
    }
}
