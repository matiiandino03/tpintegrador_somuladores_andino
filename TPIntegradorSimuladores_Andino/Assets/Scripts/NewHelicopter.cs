using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewHelicopter : MonoBehaviour
{
    CanvasManager cm;

    private void Start()
    {
        cm = FindObjectOfType<CanvasManager>();
    }
    void Update()
    {
        FixedJoint[] fjs = this.gameObject.GetComponents<FixedJoint>();
        if(fjs.Length > 0)
        {
            cm.NewHeli();
        }
    }
}
